﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Assignment
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "FilterByCategory",
                url: "Category/{categoryName}",
                defaults: new { controller = "Home", action = "FilterByCategory" }
                );

            routes.MapRoute(
               name: "FilterByBlogger",
               url: "IdentityModels/{Name}",
               defaults: new { controller = "AccountController", action = "FilterByUser" }
               );

            routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
