﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment.Models;

namespace Assignment.Controllers
{
    public class CSharpController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: cSharp
        public ActionResult Index()
        {
            List<Blog> blogs = db.Blogs.Where(a => a.Category == "CSharp").OrderByDescending(a => a.CreationDate).ToList();
            return View(blogs);
        }

        public ActionResult HomePage()
        {
            List<Blog> blogs = db.Blogs.Where(a => a.Category == "CSharp").OrderByDescending(a => a.CreationDate).ToList();
            return View(blogs);
        }
    }
}



