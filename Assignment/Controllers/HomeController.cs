﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignment.Models;

namespace Assignment.Controllers
{
	public class HomeController : Controller
	{
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
		{
            List<Blog> Blogs = db.Blogs.OrderByDescending(b => b.CreationDate).Take(5).ToList();
            return View(Blogs);
		}
    }
}