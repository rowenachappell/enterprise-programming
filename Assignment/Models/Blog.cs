﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assignment.Models
{
    public class Blog
    {
        public int BlogID { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "50 Characters Max")]
        public string Heading { get; set; }
        [Required]
        public string Category { get; set; }
        [Required]
        public string Btags { get; set; }
        [Required]
        public string Content { get; set; }
        public string ImagePath { get; set; }
        public string FileName { get; set; }
        public DateTime CreationDate { get; set; }

       
        /* Careful¬ you might be loading sensitive details that might not be required¬ */
        // public Guid UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        
    }
}