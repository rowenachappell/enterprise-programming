namespace Assignment.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Assignment.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Assignment.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Assignment.Models.ApplicationDbContext";
        }

        protected override void Seed(Assignment.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            //context.Users.AddOrUpdate(new Models.ApplicationUser { /*Enter user details here */ });
            //context.Blogs.AddOrUpdate(new Models.Blog { ApplicationUser = context.Users.Where(x=> x.Email == "Something@something.com").FirstOrDefault() });
            //context.SaveChanges();


            context.Blogs.AddOrUpdate( b => b.BlogID,

                // 5 C Sharp Blogs 
                new Blog()
                {
                    BlogID = 1, Heading = "C Sharp Header", Category = "CSharp", Btags = "cSharp", Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/1.jpg",FileName = null, CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 2,
                    Heading = "C Sharp Header 2",
                    Category = "CSharp",
                    Btags = "cSharp",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/1.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },  
                new Blog()
                {
                    BlogID = 3,
                    Heading = "C Sharp Header 3",
                    Category = "CSharp",
                    Btags = "cSharp",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/1.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },

                
                new Blog()
                {
                    BlogID = 4,
                    Heading = "C Sharp Header 4",
                    Category = "CSharp",
                    Btags = "cSharp",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/1.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                
                new Blog()
                {
                    BlogID = 5,
                    Heading = "C Sharp Header 5",
                    Category = "CSharp",
                    Btags = "cSharp",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/1.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },

                // 5 CSS Blogs 

                new Blog()
                {
                BlogID = 6, Heading = "CSS Header 1", Category = "CSS", Btags = "css", Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                ImagePath = "/Images/2.jpg",FileName = null, CreationDate = DateTime.Now
                },

                new Blog()
                {
                    BlogID = 7,
                    Heading = "CSS Header 2",
                    Category = "CSS",
                    Btags = "css",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/2.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },

                new Blog()
                {
                    BlogID = 8,
                    Heading = "CSS Header 3",
                    Category = "CSS",
                    Btags = "css",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/2.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 9,
                    Heading = "CSS Header 4",
                    Category = "CSS",
                    Btags = "css",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/2.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 10,
                    Heading = "CSS Header 5",
                    Category = "CSS",
                    Btags = "css",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/2.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },



                // 5 MVC Blogs
                new Blog()
                {
                    BlogID = 11,
                    Heading = "MVC Header 1",
                    Category = "MVC",
                    Btags = "MVC",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/3.jpg", FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 12,
                    Heading = "MVC Header 2",
                    Category = "MVC",
                    Btags = "MVC",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/3.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 13,
                    Heading = "MVC Header 3",
                    Category = "MVC",
                    Btags = "MVC",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/3.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 14,
                    Heading = "MVC Header 4",
                    Category = "MVC",
                    Btags = "MVC",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/3.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 15,
                    Heading = "MVC Header 5",
                    Category = "MVC",
                    Btags = "MVC",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/3.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },


                // 5 Java Blogs 
                new Blog()
                {
                    BlogID = 16,
                    Heading = "Java Header 1 ",
                    Category = "Java",
                    Btags = "Java",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/4.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 17,
                    Heading = "Java Header 2 ",
                    Category = "Java",
                    Btags = "Java",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/4.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 18,
                    Heading = "Java Header 3 ",
                    Category = "Java",
                    Btags = "Java",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/4.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 19,
                    Heading = "Java Header 4 ",
                    Category = "Java",
                    Btags = "Java",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/4.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                },
                new Blog()
                {
                    BlogID = 20,
                    Heading = "Java Header 5 ",
                    Category = "Java",
                    Btags = "Java",
                    Content = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                    ImagePath = "/Images/4.jpg",
                    FileName = null,
                    CreationDate = DateTime.Now
                }
                );

            context.Users.AddOrUpdate(a => a.Id, 
            new ApplicationUser()
            {
                Name = "Lara",
                Surname = "Gatt",
                Desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                UserName = "LaraGatt",
                Email = "LaraGatt@gmail.com"
            },
            new ApplicationUser()
            {
                Name = "Mark",
                Surname = "Galea",
                Desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                UserName = "MarkGalea",
                Email = "MarkGalea@gmail.com"
            },
            new ApplicationUser()
            {
                Name = "Wayne",
                Surname = "Camilleri",
                Desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                UserName = "WayneCamilleri ",
                Email = "WayneCamilleri@gmail.com"
            },

            new ApplicationUser()
            {
                Name = "Vicky",
                Surname = "Cassar",
                Desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                UserName = "VickyCassar ",
                Email = "VickyCassar@gmail.com"
            },

            new ApplicationUser()
            {
                Name = "Raquela",
                Surname = "White",
                Desc = "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                UserName = "RaquelaWhite",
                Email = "RaquelaWhite@gmail.com"
            }
                );
        }
    }
}
